Repository for fluentConvert.

fluentConvert is used to convert fluent \*.msh files to FEniCS 2017.2.0 XDMF meshes

The program should be put somewhere in the folders containd by `PATH`. The program is validated to work with fenics 2017.2.0.

You can run the program with: `fluentConvert.py -i INFILE -o OUTFILE` or `fluentConvert.py --input INFILE --output OUTFILE`.
If no output is specified the program will reuse the part of the filename before the .msh extension (e.g. mesh.msh becomes mesh.xdmf).

Note that a \*.h5 file is also generated. This file actually contains all data and **should** be placed in the same folder as the xdmf file