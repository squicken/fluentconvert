#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Program for converting ANSYS meshes to FEniCS

Based on: https://github.com/mikaem/tools/blob/master/mshconvert/fluent2xml.py
"""

# Import statements
from argparse import ArgumentParser  # Parse the command line
from copy import copy  # Allow for making copies of objects
from dolfin import XDMFFile, MeshEditor, Mesh, Cell, facets, MeshFunction
from numpy import array  # For creating arrays
from os import path, system  # get path and execute cmd programs
import re  # Support for regular expressions
from pdb import set_trace as bp  # For inserting breakpoints

__author__ = 'sjeng'
__email__ = 's.quicken@maastrichtuniversity.nl'
__date__ = '2018-01-29'

# Last Modified by:   Sjeng Quicken work PC
# Last Modified time: 2018-06-07

# Initialize the command line argument parser
parser = ArgumentParser()

# Input file
parser.add_argument(
    '-i', '--input', type=str,
    help='name of the file to convert')

# Output file
parser.add_argument(
    '-o', '--output', type=str,
    help='name of the output file')

parser.add_argument(
    '-c', '--compression', action='store_true',
    help='compresses the hdf5 file')

# Parse the command line
arguments = parser.parse_args()

# Print error if no input file or file does not exist
if arguments.input is None:
    raise IOError('No input file is specified')
if not path.isfile(arguments.input):
    raise IOError(
        'The file specified by the argument --input does not exist')

in_file = arguments.input

# Check if output file is specified, otherwise set to default
if arguments.output is not None:
    out_file = arguments.output
else:
    # Get the input file name without extension
    base_name, _ = path.splitext(in_file)

    # Define the output name
    out_file = base_name + '.xdmf'

# Check compression
compression = arguments.compression

# Print information regarding the input and output
info_string = '{:10.10s}{:s}'.format('Input: ', in_file)
print(info_string)
info_string = '{:10.10s}{:s}'.format('Output: ', out_file)
print(info_string)


def fluentConvert(in_file, out_file):
    """
    Read the ANSYS Fluent format (*.msh) file

    The fluent mesh (the .msh file) is basically stored as a list of vertices,
    and then a list of faces for each zone of the mesh, the interior and the
    boundaries.

    This code is largely copied from the script that is referenced above
    """

    # Use regular expressions to identify sections in a fluent file

    re_dimline = re.compile(
        r"""
        \(2\s  # Starts with (2
        (\d)  # A digit
        \)
        """, re.VERBOSE)

    # Comments start with (0
    re_comment = re.compile(
        r"""
        \(0  # Starts with (0
        \s.*  # Anything
        """, re.VERBOSE)

    # Zones are initialized by
    re_zone_init = re.compile(
        r"""
        \(10\s\(0  # Beginning with (10 (0
        \s
        (\w+)  # Hex
        \s
        (\w+)  # Hex
        \s
        (\d+)  # Any number of digits
        \s
        (\d+)  # Any number of digits
        \)\)   # Closed by ))
        """, re.VERBOSE)

    # Subsequent zones
    re_zone = re.compile(
        r"""
        \(10\s\(  # Beginning with (10 (0
        (\w+)  # Hex
        \s
        (\w+)  # Hex
        \s
        (\w+)  # Hex
        \s
        (\d+)  # Any number of digits
        \s
        (\d)  # One digit
        \)(\(|)  # Ending on either )( or )
        """, re.VERBOSE)

    # Find first face
    re_face_init = re.compile(
        r"""
        \(13  # Beginning with (13
        (\s*)
        \(0\s+  # (0 followed by any number of spaces
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (0|0\s0)  # 0 or 0 0
        \)\)
        """, re.VERBOSE)

    # Subsequent faces
    re_face = re.compile(
        r"""
        \(13  # Starting with (13
        (\s*)  # Any number of spaces
        \(
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \)
        (\s*)  # Any number of spaces or none
        (\(|)  # Ending on ( or nothing
        """, re.VERBOSE)

    # Finding periodics, not used
    re_periodic = re.compile(
        r"""
        ^\(18.*  # Starting with (18
        \(
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \).*\(
        """, re.VERBOSE)

    # Periodic faces, not used
    re_pfaces = re.compile(
        r"((^\s)|)(\w+)(\s*)(\w+)")

    # Find first cell
    re_cells_init = re.compile(
        r"""
        \(12  # Starting with (12
        (\s*)  # Any number of spaces
        \(0
        (\s+)  # Any number of spaces
        (\w+)  # Hex
        (\s+)  # Any number of spaces
        (\w+)  # Hex
        (\s+)  # Any number of spaces
        (0|0\s0)  # 0 or 0 0
        \)\)
        """, re.VERBOSE)

    # Subsequent cells
    re_cells = re.compile(
        r"""
        \(12.*\(  # Starting with (12 (
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\d+)  # Any number of digits
        \s+
        (\d+)  # Any number of digits
        \)\)
        """, re.VERBOSE)

    # Other cells types (unsupported)
    re_cells2 = re.compile(
        r"""^\(12
        (\s*)  # Any number of spaces
        \(
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \s+
        (\w+)  # Hex
        \)
        (\s*)
        (\(|)""", re.VERBOSE)

    # Zone ids
    re_zones = re.compile(
        r"""
        \((45|39)\s+\(  # Starting with with (45 or (39
        (\d+)  # Any number of digits
        \s+
        (\S+)  # The a name
        \s+
        (\S+)  # The a name
        .*\)
        \(
        (.*|[0-9]+[\.]*[0-9]*)  # Something between brackets
        \)\)
        """, re.VERBOSE)

    # The location where things either start or stop ( (-signs or )-signs )
    re_parthesis = re.compile(
        r"""
        (^\s*\)(\s*)|  # closing bracket OR
        ^\s*\)\)(\s*)|  # two closing brackets
        ^\s*\(\s*)  # Or opening bracket
        """, re.VERBOSE)

    # Declare som maps that will be built when reading in vertices and faces:
    cell_map = {}  # Maps cell id with vertices
    boundary_cells = {}  # List of cells attached to a boundary facet.
    zones = {}  # zone information (not really used yet)

    def read_periodic(ifile, periodic_dx):
        """Scan past periodic section. Periodicity is computed by FEniCS."""
        while 1:
            line = ifile.readline()
            a = re.search(re_pfaces, line)
            if a:
                continue
            break

    def read_zone_vertices(dim, Nmin, Nmax, ifile, editor):
        """Scan ifile for vertices and add to mesh_editor."""
        # First line could be either just "(" or a regular vertex. Check for
        # initial paranthesis. If paranthesis then read a new line, else reset
        pos = ifile.tell()
        line = ifile.readline()
        if not re.search(re_parthesis, line):
            ifile.seek(pos)  # reset
        # read Nmax-Nmin vertices
        for i in range(Nmin, Nmax + 1):
            line = ifile.readline()
            vertex = [eval(x) for x in line.split()]
            if dim == 2:
                editor.add_vertex(i - Nmin, vertex[0], vertex[1])
            else:
                editor.add_vertex(i - Nmin, vertex[0], vertex[1], vertex[2])

    def read_faces(zone_id, Nmin, Nmax, bc_type, face, ifile):
        """Read all faces and create cell_map + boundary maps."""
        pos = ifile.tell()  # current position
        line = ifile.readline()
        # check for initial paranthesis. If paranthesis then read a new line,
        # else reset
        if not re.search(re_parthesis, line):
            ifile.seek(pos)

        # read Nmax-Nmin faces
        for i in range(Nmin, Nmax + 1):
            line = ifile.readline()
            ln = line.split()
            if face == 0:
                nd = int(ln[0], 16)  # Number of vertices
                nds = [int(x, 16) for x in ln[1:(nd + 1)]]
                cells = [int(x, 16) for x in ln[(nd + 1):]]
            else:
                nd = face
                nds = [int(x, 16) for x in ln[:nd]]
                cells = [int(x, 16) for x in ln[nd:]]

            if min(cells) == 0:  # A boundary zone
                if zone_id in boundary_cells:
                    boundary_cells[zone_id][max(cells)] = array(nds)
                else:
                    boundary_cells[zone_id] = {max(cells): array(nds)}

            for c in cells:
                if c > 0:
                    if c not in cell_map:
                        cell_map[c] = copy(nds)
                    else:
                        cell_map[c] = list(set(cell_map[c] + nds))

    def scan_fluent_mesh(ifile, mesh, editor):
        """Scan fluent mesh and generate maps."""
        dim = 0  # Initialize dim
        one = 0  # Initialize one

        while 1:  # Stay in loop as long no break is encountered

            # Start reading the mesh file
            line = ifile.readline()

            if len(line) == 0:
                # Reached the end of the file
                info_string = (
                    'Finished reading file')
                print(info_string)

                break

            if dim == 0:  # Dimension usually comes first
                # Use the dimline regexp to find the dimension
                a = re.search(re_dimline, line)
                if a:
                    info_string = (
                        'Reading dimensions')
                    print(info_string)

                    # First group denotes the dimension
                    dim = int(a.group(1))

                    # Create a mesh of this dimension
                    editor.open(mesh, 'tetrahedron', dim, dim)
                    continue

            if one == 0:  # The total number of vertices
                # Use the re_zone_init regexp to find the dimension
                a = re.search(re_zone_init, line)
                if a:
                    info_string = (
                        'Reading zone info')
                    print(info_string)

                    one, num_vertices, = (
                        int(a.group(1)),
                        int(a.group(2), 16)  # Hex
                        )

                    # Initialize the number of vertices in the mesh
                    editor.init_vertices(num_vertices)
                    continue

            # Start reading the lines and search for vertices
            a = re.search(re_zone, line)
            if a:
                # Find the length and location  of the vertices
                zone_id, first_id, last_id = (
                    int(a.group(1), 16),  # Hex
                    int(a.group(2), 16),  # Hex
                    int(a.group(3), 16),  # Hex
                    )

                info_string = (
                    'Reading {:d} vertices in zone {:d}').format(
                    last_id - first_id + 1, zone_id + 1)
                print(info_string)

                # Read the actual vertices
                read_zone_vertices(dim, first_id, last_id, ifile, editor)
                continue

            # Skip the next zone?
            a = re.search(re_zones, line)
            if a:
                info_string = (
                    'Reading zone info {:s}').format(line.rstrip())
                print(info_string)

                zone_id, zone_type, zone_name, radius = (
                    int(a.group(2)),
                    a.group(3),
                    a.group(4),
                    a.group(5),
                    )
                zones[zone_id] = [zone_type, zone_name, radius]
                continue

            # Get total number of cells/elements
            a = re.search(re_cells_init, line)
            if a:
                info_string = ('Reading cell info {:s}').format(line.rstrip())
                print(info_string)

                first_id, tot_num_cells = int(
                    a.group(3), 16), int(a.group(5), 16)

                # Initialize the cells in the mesh
                editor.init_cells(tot_num_cells)
                continue

            a = re.search(re_cells, line)  # Get the cell info.
            if a:
                zone_id, first_id, last_id, bc_type = (
                    int(a.group(1), 16),  # Hex
                    int(a.group(2), 16),  # Hex
                    int(a.group(3), 16),  # Hex
                    int(a.group(4), 16),  # Hex
                    )

                info_string = (
                    'Reading {:d} faces in zone {:d}').format(
                    last_id - first_id + 1, zone_id + 1)
                print(info_string)

                if last_id == 0:
                    raise TypeError("Zero elements!")
                continue

            # Get the cell info of possible other cell types
            a = re.search(re_cells2, line)
            if a:
                raise TypeError(
                    "Wrong cell type. Can only handle one single cell type")

            a = re.search(re_face_init, line)
            if a:
                'Reading total number of faces', line
                continue

            a = re.search(re_face, line)
            if a:
                info_string = ('Reading faces {:s}').format(line.rstrip())
                print(info_string)
                zone_id, first_id, last_id, bc_type, face_type = (
                    int(a.group(2), 16),  # Hex
                    int(a.group(3), 16),  # Hex
                    int(a.group(4), 16),  # Hex
                    int(a.group(5), 16),  # Hex
                    int(a.group(6), 16),  # Hex
                    )

                # Read the faces
                read_faces(zone_id, first_id, last_id,
                           bc_type, face_type, ifile)
                continue

            # This will be skipped
            a = re.search(re_periodic, line)
            if a:
                info_string = (
                    'Scanning past periodic connectivity {:s}'). \
                    format(line.rstrip())
                print(info_string)
                read_periodic(ifile, periodic_dx)
                continue

            if any([re.search(st, line) for st in
                    (re_parthesis, re_comment)]) or not line.strip():
                continue

            # Should not make it here
            raise IOError('Something went wrong reading fluent mesh.')

    def write_fenics_file(ofile, mesh, editor):

        dim = mesh.geometry().dim()
        for i in range(1, len(cell_map) + 1):
            if dim == 2:
                editor.add_cell(
                    i - 1, cell_map[i][0] - 1, cell_map[i][1] - 1,
                    cell_map[i][2] - 1)
            else:
                editor.add_cell(
                    i - 1, cell_map[i][0] - 1, cell_map[i][1] - 1,
                    cell_map[i][2] - 1, cell_map[i][3] - 1)

        mesh.order()
        # Set MeshValueCollections from info in  boundary_cell
        md = mesh.domains()
        for zone, cells in boundary_cells.items():
            for cell, nds in cells.items():
                dolfin_cell = Cell(mesh, cell - 1)
                vertices_of_face = nds - 1
                for jj, ff in enumerate(facets(dolfin_cell)):
                    facet_vertices = ff.entities(0)
                    if all(map(lambda x: x in vertices_of_face,
                               facet_vertices)):
                        break
                md.set_marker((ff.index(), zone), dim - 1)

        # Initialize the boundaries
        D = mesh.topology().dim()
        boundaries = MeshFunction("size_t", mesh, D - 1, mesh.domains())
        boundaries.rename('boundaries', 'boundaries')

        # Write the mesh to XDMF
        xdmfMesh = XDMFFile(mesh.mpi_comm(), out_file)
        xdmfMesh.write(mesh)
        xdmfMesh.write(boundaries)
        info_string = ('Finished writing FEniCS mesh')
        print(info_string)

    ifile = open(in_file, "r")
    ofile = XDMFFile(out_file)
    mesh = Mesh()
    editor = MeshEditor()

    scan_fluent_mesh(ifile, mesh, editor)
    write_fenics_file(ofile, mesh, editor)

    ifile.close()


# Actually call the whole thing with the input given above
fluentConvert(in_file, out_file)

# If compression is enabled repack the hdf5 file to a compressed file
if compression:
    # Define h5 filename
    filename1 = path.splitext(out_file)[0] + '.h5'
    filename2 = path.splitext(out_file)[0] + '.h5.tmp'
    compression_cmd = (
        'h5repack  -f GZIP=1 {:s} {:s}'.format(
            filename1, filename2))
    remove_cmd = (
        'rm {:s} & mv {:s} {:s}'. format(
            filename1, filename2, filename1))
    # Execute command line compression
    system(compression_cmd)
    system(remove_cmd)
